//
//  WeatherService.swift
//  Weather Table
//
//  Created by Rostislav Zapolsky on 7/8/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation
import Moya

enum WeatherService {
    static private let units = "metric"
    static private let appKey = "5b3d87f4e304e0991a34cb7c993c1d26"
    
    case getWeather(id: String)
}

// MARK: - TargetType Protocol Implementation
extension WeatherService: TargetType {
    
    var baseURL: URL { return URL(string: "https://api.openweathermap.org/data/2.5/forecast")! }
    
    var path: String {
        switch self {
        case .getWeather:
            return ""
        }
    }
    var method: Moya.Method {
        switch self {
        case .getWeather:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case let .getWeather(id):
            return .requestParameters(parameters: ["id": id,
                                                   "units": WeatherService.units,
                                                   "APPID": WeatherService.appKey],
                                      encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getWeather:
            return Data()
        }
    }
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
