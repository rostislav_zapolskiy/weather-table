//
//  WeatherTableViewController.swift
//  Weather Table
//
//  Created by Rostislav Zapolsky on 7/8/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

class WeatherTableViewController: UITableViewController {
    
    // MARK: Properties
    var weatherInfo = [WeatherInfo]()
    let provider = MoyaProvider<WeatherService>()
    
    // MARK: Background methods
    func loadWeather() {
        provider.request(.getWeather(id: "625144")) { result in
            switch result {
            case .success(let response):
                self.updateWeather(with: response.data)
                self.tableView.reloadData()
            case .failure(let error):
                print("HHTP request error: \(error)")
            }
        }
    }
    
    func updateWeather(with data: Data) {
        do {
            let json = try JSON(data: data)
            guard let list = json["list"].array else { return }
            weatherInfo.removeAll()
            
            for element in list {
                guard let date = element["dt"].double,
                    let temperature = element["main"]["temp"].double else { continue }
                weatherInfo.append(WeatherInfo(date: Date(timeIntervalSince1970: date), temperature: temperature))
            }
        } catch {
            print("JSON parcing error \(error)")
        }
    }
    
    // MARK: UITableViewController methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherInfo.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherInfoItem")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        let dateString = dateFormatter.string(from: weatherInfo[indexPath.row].date)
        
        cell?.textLabel?.text = dateString + " " + String(weatherInfo[indexPath.row].temperature) + "°C"
        return cell ?? UITableViewCell(style: .default, reuseIdentifier: "WeatherInfoItem")
    }
}
