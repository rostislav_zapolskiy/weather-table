//
//  ЦуферукШтащ.swift
//  Weather Table
//
//  Created by Rostislav Zapolsky on 7/8/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import Foundation

struct WeatherInfo {
    var date: Date
    var temperature: Double
}
