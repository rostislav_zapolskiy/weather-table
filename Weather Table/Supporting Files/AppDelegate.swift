//
//  AppDelegate.swift
//  Weather Table
//
//  Created by Rostislav Zapolsky on 7/8/19.
//  Copyright © 2019 Rostislav Zapolsky. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.setMinimumBackgroundFetchInterval(3600)
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let viewController = window?.rootViewController as? WeatherTableViewController {
            viewController.loadWeather()
        }
    }
}

